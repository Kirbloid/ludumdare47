extends Node2D

var _title_screen = preload("res://Scenes/Title.tscn")
var _game = preload("res://Scenes/Game.tscn")

var current_scene
var ending = 0


func _ready():
	current_scene = _title_screen.instance()
	ChangeScene(_game)


func ChangeScene(scene):
	current_scene.queue_free()
	current_scene = scene.instance()
	add_child(current_scene)
