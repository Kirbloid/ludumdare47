extends Area2D


enum SpaceType {
	HOME,
	BASIC,
	LANDSCAPE,
	GEM_MINE,
	MAGIC_PORTAL,
	CLOSED_PATH,
	OPEN_PATH,
	WATER,
	WILDERNESS,
	MONSTER
}

onready var game = get_node("/root/Main/Game")

export(int) var hearts_worth
export(int) var gems_worth
export(int) var hearts_upgrade_cost
export(int) var gems_upgrade_cost
export(SpaceType) var space_type
export var space_name = ""

var next_main_space
var sub_path_space = null
var level = 1
var max_level = 10
var has_opened = false


func _process(delta):
	$Label.text = str(level)


func get_next_main_board_space():
	if get_index() < game.main_board_spaces.size() - 1:
		next_main_space = game.main_board_spaces[get_index() + 1]
	else:
		next_main_space = game.main_board_spaces[0]


func get_next_sub_board_space(sub_board):
	if get_index() < sub_board.size() - 1:
		next_main_space = sub_board[get_index() + 1]


func step_on():
	if space_type == SpaceType.LANDSCAPE:
		$LandscapeDownSprite.set_visible(true)
		$LandscapeUpSprite.set_visible(false)
	elif space_type == SpaceType.GEM_MINE:
		$GemMineDownSprite.set_visible(true)
		$GemMineUpSprite.set_visible(false)
	elif space_type == SpaceType.MAGIC_PORTAL:
		$PortalDownSprite.set_visible(true)
		$PortalUpSprite.set_visible(false)
	else:
		$SpaceDownSprite.set_visible(true)
		$SpaceUpSprite.set_visible(false)


func step_off():
	if space_type == SpaceType.LANDSCAPE:
		$LandscapeDownSprite.set_visible(false)
		$LandscapeUpSprite.set_visible(true)
	elif space_type == SpaceType.GEM_MINE:
		$GemMineDownSprite.set_visible(false)
		$GemMineUpSprite.set_visible(true)
	elif space_type == SpaceType.MAGIC_PORTAL:
		$PortalDownSprite.set_visible(false)
		$PortalUpSprite.set_visible(true)
	else:
		$SpaceDownSprite.set_visible(false)
		$SpaceUpSprite.set_visible(true)


func upgrade():
	level += 1


func unlock_path():
	space_type = SpaceType.OPEN_PATH
	sub_path_space.get_parent().set_visible(true)
	if sub_path_space.get_parent().name == "SubPath2":
		game.get_node("Monster").visible = true
	game.ui.update_menu(space_type)
	has_opened = true


func renovate_to_landscape():
	space_type = SpaceType.LANDSCAPE
	hearts_worth = 10
	gems_worth = 0
	hearts_upgrade_cost = 25
	gems_upgrade_cost = 5
	level = 1
	space_name = "Landscape"
	turn_off_all_visibility()
	$LandscapeDownSprite.set_visible(true)


func renovate_to_gem_mine():
	space_type = SpaceType.GEM_MINE
	hearts_worth = 0
	gems_worth = 10
	hearts_upgrade_cost = 50
	gems_upgrade_cost = 10
	level = 1
	space_name = "Gem Mine"
	turn_off_all_visibility()
	$GemMineDownSprite.set_visible(true)


func renovate_to_portal():
	space_type = SpaceType.MAGIC_PORTAL
	hearts_worth = 0
	gems_worth = 0
	hearts_upgrade_cost = 0
	gems_upgrade_cost = 20
	level = 1
	space_name = "Magic Portal"
	turn_off_all_visibility()
	$PortalDownSprite.set_visible(true)


func turn_off_all_visibility():
	$LandscapeDownSprite.set_visible(false)
	$LandscapeUpSprite.set_visible(false)
	$GemMineDownSprite.set_visible(false)
	$GemMineUpSprite.set_visible(false)
	$PortalDownSprite.set_visible(false)
	$PortalUpSprite.set_visible(false)
	$SpaceDownSprite.set_visible(false)
	$SpaceUpSprite.set_visible(false)


func _on_Space_body_entered(body):
	if body.name == "Monster":
		space_type = SpaceType.MONSTER
		step_on()
	if body.name == "Player":
		if space_type == SpaceType.MONSTER and body.left_to_move < 1:
			pass
		else:
			#body.set_current_space(self)
			body.set_next_space(next_main_space)
			step_on()


func _on_Space_body_exited(body):
	if body.name == "Monster":
		if gems_worth == 0:
			if has_opened:
				space_type = SpaceType.OPEN_PATH
			else:
				space_type = SpaceType.CLOSED_PATH
		else:
			space_type = SpaceType.WILDERNESS
		step_off()
	if body.name == "Player":
		step_off()
