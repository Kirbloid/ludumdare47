extends KinematicBody2D


onready var game = get_parent()

var current_space
var next_space
var moving_piece = false
var interpolate_t = 0
var left_to_move = 0
var walk_path : Array
var walk_path_max_size = 9

func _ready():
	walk_path = game.get_node("Board/SubPath2").get_children()
	current_space = walk_path[walk_path_max_size]
	position = current_space.global_position


func _physics_process(delta):
	if moving_piece:
		interpolate_t += delta * 3.55
		$SpriteHolder.global_position = global_position.linear_interpolate(next_space.position, interpolate_t)


func next_space():
	moving_piece = true
	var current_space_index = current_space.get_index()
	if current_space_index == 0:
		next_space = walk_path[walk_path_max_size]
	else:
		next_space = walk_path[current_space_index - 1]
	$AnimationPlayer.play("HopToSpace")


func move(number_of_spaces):
	left_to_move = number_of_spaces
	next_space()


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "HopToSpace":
		$SpriteHolder.position = Vector2.ZERO
		position = next_space.global_position
		$AnimationPlayer.play("ResetToZero")
		moving_piece = false
		interpolate_t = 0
		left_to_move -= 1
		current_space = next_space
		if left_to_move > 0:
			next_space()
