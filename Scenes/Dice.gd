extends KinematicBody2D


onready var game = get_parent()
onready var side_sprites = get_node("Sides").get_children()

var value
var velocity
var trigger_move = false


func _ready():
	velocity = Vector2(125, -25)
	$AnimationPlayer.play("Throw")


func _physics_process(delta):
	if $Roll.time_left > 0.7:
		move_and_slide(velocity)
	else:
		for side in side_sprites:
			side.set_visible(false)
		side_sprites[value - 1].set_visible(true)
		if trigger_move == false:
			game.player_move(value)
			trigger_move = true


func set_dice_value(die_roll):
	value = die_roll


func randomize_sprite_side():
	for side in side_sprites:
		side.set_visible(false)
	side_sprites[game.rng.randi_range(0, 5)].set_visible(true)


func _on_AnimationPlayer_animation_finished(anim_name):
	queue_free()
