extends Control


onready var game = get_parent()
onready var hearts_label = get_node("HUD/Currency/Hearts")
onready var gems_label = get_node("HUD/Currency/Gems")
onready var dialogue_label = get_node("HUD/DialogueMenu/Dialogue")
onready var menus = $HUD/DialogueMenu/Menus.get_children()
onready var menu_cursor = get_node("HUD/DialogueMenu/MenuCursor")
onready var dialogue_button = get_node("HUD/DialogueMenu/AdvanceBlink")

var hearts = 0
var gems = 0
var current_menu = 0
var menu_max = 2
var menu_positions : Array
var dialogue_queue


func _ready():
	update_labels()
	update_menu(9)
	get_node("FairyAnimation").play("FairyIdle")
	get_node("BlinkAnimation").play("DialogueBlink")
	get_node("MenuCursorAnimation").play("MenuCursorBlink")
	menu_positions.append(get_node("HUD/DialogueMenu/MenuPos1"))
	menu_positions.append(get_node("HUD/DialogueMenu/MenuPos2"))
	menu_positions.append(get_node("HUD/DialogueMenu/MenuPos3"))


func update_menu_cursor(menu):
	$HUD/DialogueMenu/MenuCursor.position = menu_positions[menu].position


func update_labels():
	hearts_label.text = str(hearts)
	gems_label.text = str(gems)


func update_menu(space_type):
	for menu in menus:
		menu.visible = false
	match space_type:
		0:
			$HUD/DialogueMenu/Menus/MenuHome.visible = true
			menu_max = 2
		1:
			$HUD/DialogueMenu/Menus/MenuStandard.visible = true
			menu_max = 2
		2:
			$HUD/DialogueMenu/Menus/MenuStandard.visible = true
			menu_max = 2
		3:
			$HUD/DialogueMenu/Menus/MenuStandard.visible = true
			menu_max = 2
		4:
			$HUD/DialogueMenu/Menus/MenuPortal.visible = true
			menu_max = 2
		5:
			$HUD/DialogueMenu/Menus/MenuLockedPath.visible = true
			menu_max = 1
		6:
			$HUD/DialogueMenu/Menus/MenuOpenPath.visible = true
			menu_max = 1
		7:
			$HUD/DialogueMenu/Menus/MenuSubSpace.visible = true
			menu_max = 1
		8:
			$HUD/DialogueMenu/Menus/MenuSubSpace.visible = true
			menu_max = 1
		9:
			$HUD/DialogueMenu/Menus/MenuIntro.visible = true
			menu_max = 0
	current_menu = 0
	$HUD/DialogueMenu/Menus/Info1.visible = true
	$HUD/DialogueMenu/MenuCursor.position = $HUD/DialogueMenu/MenuPos1.position


func activate_renovate_menu():
	for menu in menus:
		menu.visible = false
	$HUD/DialogueMenu/Menus/MenuRenovate.visible = true
	$HUD/DialogueMenu/Menus/Info1.visible = true


func add_hearts(amount):
	hearts += amount


func add_gems(amount):
	gems += amount


func update_info(desc1, desc2 = null):
	$HUD/DialogueMenu/Menus/Info1.text = desc1
	if desc2 == null:
		$HUD/DialogueMenu/Menus/Info2.text = desc1
	else:
		$HUD/DialogueMenu/Menus/Info2.text = desc2


func _on_Timer_timeout():
	# Swap Info
	if $HUD/DialogueMenu/Menus/Info1.is_visible():
		$HUD/DialogueMenu/Menus/Info1.visible = false
		$HUD/DialogueMenu/Menus/Info2.visible = true
	else:
		$HUD/DialogueMenu/Menus/Info2.visible = false
		$HUD/DialogueMenu/Menus/Info1.visible = true


func toggle_dialogue():
	if dialogue_label.is_visible():
		dialogue_button.visible = false
		dialogue_label.visible = false
		$HUD/DialogueMenu/Menus.visible = true
		menu_cursor.visible = true
	else:
		dialogue_button.visible = true
		dialogue_label.visible = true
		$HUD/DialogueMenu/Menus.visible = false
		menu_cursor.visible = false


func display_next_dialogue():
	dialogue_label.text = dialogue_queue
	dialogue_queue = null


func intro_dialogue():
	toggle_dialogue()
	dialogue_label.text = "That was it? There was barely anything out there..."
	dialogue_queue = "I think we can improve a ton of things on the road. Let's get to it!"


func ending_dialogue():
	game.unlocked_leave = true
	toggle_dialogue()
	dialogue_label.text = "Thank you for playing! Your game took " + str(game.turns_taken) + " turns to complete."
	dialogue_queue = "Be sure to comment and rate on Ludum Dare 47!"
