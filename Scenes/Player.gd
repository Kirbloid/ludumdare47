extends KinematicBody2D


onready var game = get_parent()

var current_space
var next_space
var moving_piece = false
var interpolate_t = 0
var left_to_move = 0


func _ready():
	position = game.get_node("Board/MainSpaces/HomeSpace").global_position
	current_space = game.get_node("Board/MainSpaces/HomeSpace")


func _physics_process(delta):
	if moving_piece:
		interpolate_t += delta * 3.55
		$SpriteHolder.global_position = global_position.linear_interpolate(next_space.position, interpolate_t)


func next_space():
	moving_piece = true
	$AnimationPlayer.play("HopToSpace")


func set_next_space(new_space):
	next_space = new_space
	if position.x > next_space.position.x:
		$SpriteHolder/Sprite.flip_h = true
	else:
		$SpriteHolder/Sprite.flip_h = false


func move_player(number_of_spaces):
	left_to_move = number_of_spaces
	next_space()


func take_sub_path():
	next_space = current_space.sub_path_space


func teleport():
	move_player(current_space.level)


func teleport_home():
	next_space = game.get_node("Board/MainSpaces/HomeSpace")
	next_space()


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "HopToSpace":
		left_to_move -= 1
		$SpriteHolder.position = Vector2.ZERO
		position = next_space.global_position
		$AnimationPlayer.play("ResetToZero")
		moving_piece = false
		interpolate_t = 0
		if left_to_move > 0:
			next_space()
		else:
			current_space = next_space
			game.activate_space(current_space)
			game.can_roll = true
			game.update_descriptions()
			game.turns_taken += 1
