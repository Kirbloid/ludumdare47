extends Node2D

#var _ending = preload("res://Scenes/Ending.tscn")
var _dice = preload("res://Scenes/Dice.tscn")

onready var player = get_node("Player")
onready var monster = get_node("Monster")
onready var ui = get_node("UI")

var main
var main_board_spaces : Array
var sub_path_1_spaces : Array
var sub_path_2_spaces : Array
var sub_path_3_spaces : Array
var sub_path_4_spaces : Array

var rng = RandomNumberGenerator.new()

# Player stats
var can_roll = true
var renovating = false

# Cutscene Flags
var is_intro = true
var turns_taken = 0
var first_space_after_intro = false
var first_roll_after_intro = false
var first_upgrade = false
var first_landscape = false
var first_gem_mine = false
var first_magic_portal = false
var unlocked_lake_path = false
var unlocked_wilderness_path = false
var unlocked_ocean_path = false
var unlocked_leave = false
var leave_hearts_cost = 1000
var leave_gems_cost = 1000


func _ready():
	rng.randomize()
	update_descriptions()
	main = get_node("/root/Main")
	main_board_spaces = get_node("/root/Main/Game/Board/MainSpaces").get_children()
	for space in main_board_spaces:
		space.get_next_main_board_space()
	
	sub_path_1_spaces = get_node("/root/Main/Game/Board/SubPath1").get_children()
	for space in sub_path_1_spaces:
		space.get_next_sub_board_space(sub_path_1_spaces)
	sub_path_2_spaces = get_node("/root/Main/Game/Board/SubPath2").get_children()
	for space in sub_path_2_spaces:
		space.get_next_sub_board_space(sub_path_2_spaces)
	sub_path_3_spaces = get_node("/root/Main/Game/Board/SubPath3").get_children()
	for space in sub_path_3_spaces:
		space.get_next_sub_board_space(sub_path_3_spaces)
	sub_path_4_spaces = get_node("/root/Main/Game/Board/SubPath4").get_children()
	for space in sub_path_4_spaces:
		space.get_next_sub_board_space(sub_path_4_spaces)
	
	# hacks for getting sub paths
	main_board_spaces[3].sub_path_space = sub_path_1_spaces[0]
	sub_path_1_spaces[1].next_main_space = main_board_spaces[13]
	main_board_spaces[11].sub_path_space = sub_path_2_spaces[0]
	sub_path_2_spaces[9].next_main_space = main_board_spaces[17]
	sub_path_2_spaces[8].sub_path_space = sub_path_3_spaces[0]
	sub_path_3_spaces[4].next_main_space = main_board_spaces[19]
	main_board_spaces[0].sub_path_space = sub_path_4_spaces[0]
	sub_path_4_spaces[3].next_main_space = sub_path_4_spaces[3]


func _process(delta):
	if can_roll and not ui.dialogue_label.is_visible():
		
		### DEBUG - REMOVE! ###
#		if Input.is_action_just_pressed("1"):
#			roll_debug_dice(1)
#		elif Input.is_action_just_pressed("2"):
#			roll_debug_dice(2)
#		elif Input.is_action_just_pressed("3"):
#			roll_debug_dice(3)
#		elif Input.is_action_just_pressed("4"):
#			roll_debug_dice(4)
#		elif Input.is_action_just_pressed("5"):
#			roll_debug_dice(5)
#		elif Input.is_action_just_pressed("6"):
#			roll_debug_dice(6)
		### ### ### ### ### ###
		
		if Input.is_action_just_pressed("ui_down"):
			if ui.current_menu < ui.menu_max:
				ui.current_menu += 1
				ui.update_menu_cursor(ui.current_menu)
				update_descriptions()
		elif Input.is_action_just_pressed("ui_up"):
			if ui.current_menu > 0:
				ui.current_menu -= 1
				ui.update_menu_cursor(ui.current_menu)
				update_descriptions()
		elif Input.is_action_just_pressed("ui_accept"):
			if renovating:
				do_renovate_action(ui.current_menu)
			else:
				do_menu_action(ui.current_menu)
			$Sound.play()
	elif ui.dialogue_label.is_visible():
		if Input.is_action_just_pressed("ui_accept"):
			if ui.dialogue_queue == null and not unlocked_leave:
				ui.toggle_dialogue()
			elif not ui.dialogue_queue == null:
				ui.display_next_dialogue()
			else:
				ui.dialogue_button.visible = false


func do_menu_action(menu_action):
	if menu_action == 0:
		if player.current_space.space_type == 4:
			can_roll = false
			player.teleport()
		else:
			roll_dice()
	elif menu_action == 1:
		match player.current_space.space_type:
			0:
				attempt_upgrade(player.current_space)
			1:
				attempt_upgrade(player.current_space)
			2:
				attempt_upgrade(player.current_space)
			3:
				attempt_upgrade(player.current_space)
			4:
				attempt_upgrade(player.current_space)
			5:
				attempt_path_unlock(player.current_space)
			6:
				player.take_sub_path()
				roll_dice()
			7:
				attempt_upgrade(player.current_space)
			8:
				attempt_upgrade(player.current_space)
	else:
		match player.current_space.space_type:
			0:
				attempt_leave()
			1:
				switch_to_renovate(player.current_space)
			2:
				switch_to_renovate(player.current_space)
			3:
				switch_to_renovate(player.current_space)
			4:
				switch_to_renovate(player.current_space)


func roll_dice():
	var monster_move = rng.randi_range(1, 3)
	monster.move(monster_move)
	can_roll = false
	var rolled_value = rng.randi_range(0, 5)
	var rolled_die = _dice.instance()
	rolled_die.position = player.position
	
	match rolled_value:
		0:
			rolled_die.set_dice_value(1)
		1:
			rolled_die.set_dice_value(2)
		2:
			rolled_die.set_dice_value(3)
		3:
			rolled_die.set_dice_value(4)
		4:
			rolled_die.set_dice_value(5)
		5:
			rolled_die.set_dice_value(6)
	
	# Special case for intro
	if is_intro:
		var spaces_left = main_board_spaces.size() - player.next_space.get_index() + 1
		if spaces_left == 28:
			spaces_left = 1
		if spaces_left <= 6:
			rolled_die.set_dice_value(spaces_left)
			is_intro = false
			ui.intro_dialogue()
		
	
	add_child(rolled_die)


func roll_debug_dice(roll):
	can_roll = false
	var rolled_die = _dice.instance()
	rolled_die.position = player.position
	rolled_die.set_dice_value(roll)
	
	add_child(rolled_die)


func activate_space(current_space):
	if current_space.space_type == 9:
		player.teleport_home()
	else:
		ui.add_hearts(current_space.hearts_worth * current_space.level)
		ui.add_gems(current_space.gems_worth * current_space.level)
		ui.update_labels()
		if is_intro:
			ui.update_menu(9)
		else:
			ui.update_menu(current_space.space_type)


func player_move(spaces):
	player.move_player(spaces)


func attempt_upgrade(space):
	if space.level == space.max_level:
		ui.update_info(space.space_name + " is at max level")
	elif ui.hearts < space.hearts_upgrade_cost:
		ui.update_info("Not enough hearts...")
	elif ui.gems < space.gems_upgrade_cost:
		ui.update_info("Not enough gems...")
	else:
		ui.hearts -= space.hearts_upgrade_cost
		ui.gems -= space.gems_upgrade_cost
		ui.update_labels()
		space.upgrade()
		ui.update_info(space.space_name + " is now level " + str(space.level))


func attempt_path_unlock(space):
	var gems_cost = 50
	if ui.gems < gems_cost:
		ui.update_info("Can't afford this path unlock")
	else:
		ui.gems -= gems_cost
		space.unlock_path()
		ui.update_info("Path is unlocked")


func attempt_leave():
	if ui.hearts < leave_hearts_cost and ui.gems < leave_gems_cost:
		ui.update_info("Not enough hearts or gems...")
	else:
		# ENDING
		unlocked_leave = true
		get_node("/root/Main/Game/Board/SubPath4").visible = true
		player.next_space = get_node("Board/SubPath4/Space")
		player.move_player(4)
		ui.ending_dialogue()


func switch_to_renovate(space):
	renovating = true
	ui.activate_renovate_menu()


func do_renovate_action(menu_action):
	renovating = false
	match menu_action:
		0:
			if ui.hearts >= 15:
				ui.hearts -= 15
				player.current_space.renovate_to_landscape()
				ui.update_labels()
			else:
				ui.update_info("Can't afford this renovation")
			ui.update_menu(player.current_space.space_type)
		1:
			if ui.hearts >= 30:
				ui.hearts -= 30
				player.current_space.renovate_to_gem_mine()
				ui.update_labels()
			else:
				ui.update_info("Can't afford this renovation")
			ui.update_menu(player.current_space.space_type)
		2:
			if ui.gems >= 10:
				ui.gems -= 10
				player.current_space.renovate_to_portal()
				ui.update_labels()
			else:
				ui.update_info("Can't afford this renovation")
			ui.update_menu(player.current_space.space_type)


func update_descriptions():
	if is_intro:
		match turns_taken:
					0:
						ui.update_info("The path to adventure awaits!")
					1:
						ui.update_info("Wow it's a meadow!")
					2:
						ui.update_info("The hills sure are hilly")
					3:
						ui.update_info("Wow it's another meadow")
					4:
						ui.update_info("Can I eat these flowers?")
					5:
						ui.update_info("...")
					6:
						ui.update_info("That flower made me sick")
					7:
						ui.update_info("...")
					8:
						ui.update_info("It's ALL JUST MEADOWS!?")
					_:
						ui.update_info("...")
	elif renovating:
		match ui.current_menu:
			0: # 
				ui.update_info("Gain 10 hearts for every level", "Costs 15 hearts")
			1:
				ui.update_info("Gain 10 gems for every level", "Costs 30 hearts")
			2:
				ui.update_info("Move ahead 1 space per level", "Costs 10 gems")
	else:
		match player.current_space.space_type:
			0: # Home
				match ui.current_menu:
					0:
						ui.update_info("Home sweet home")
					1:
						ui.update_info("Upgrade home to increase rewards", "Costs " + str(player.current_space.hearts_upgrade_cost) + " hearts and " + str(player.current_space.gems_upgrade_cost) + " gems")
					2:
						ui.update_info("Let's leave home for new lands...", "Need "+ str(leave_hearts_cost) + " hearts and " + str(leave_gems_cost) + " gems")
			1: # Basic Meadow
				match ui.current_menu:
					0:
						ui.update_info("A pleasant meadow...", "Not much else here")
					1:
						ui.update_info("Upgrade to boost hearts gained", "Costs " + str(player.current_space.hearts_upgrade_cost) + " hearts")
					2:
						ui.update_info("Change the space and rewards", "Resets space to level 1")
			2: # Landscape
				match ui.current_menu:
					0:
						ui.update_info("Rolling hills and lush forests")
					1:
						ui.update_info("Upgrade to boost hearts gained", "Costs " + str(player.current_space.hearts_upgrade_cost) + " hearts and " + str(player.current_space.gems_upgrade_cost) + " gems")
					2:
						ui.update_info("Change the space and rewards", "Resets space to level 1")
			3: # Gem Mine
				match ui.current_menu:
					0:
						ui.update_info("A gem mine filled with gems")
					1:
						ui.update_info("Upgrade to boost gems gained", "Costs " + str(player.current_space.hearts_upgrade_cost) + " hearts and " + str(player.current_space.gems_upgrade_cost) + " gems")
					2:
						ui.update_info("Change the space and rewards", "Resets space to level 1")
			4: # Magic Portal
				match ui.current_menu:
					0:
						ui.update_info("Moves you ahead " + str(player.current_space.level) + " spaces")
					1:
						ui.update_info("Upgrade to boost spaces moved", "Costs " + str(player.current_space.gems_upgrade_cost) + " gems")
					2:
						ui.update_info("Change the space and rewards", "Resets space to level 1")
			5: # Closed path
				match ui.current_menu:
					0:
						ui.update_info("Something is different here", "Might be worth investigating")
					1:
						ui.update_info("Open up a branching path", "Costs 50 gems")
			6: # Open path
				match ui.current_menu:
					0:
						ui.update_info("There's another path...", "Unless you don't want to take it")
					1:
						ui.update_info("Take the branching path")
			7: # Water
				match ui.current_menu:
					0:
						ui.update_info("The water sparkles with gems")
					1:
						ui.update_info("Upgrade to boost gems gained", "Costs " + str(player.current_space.hearts_upgrade_cost) + " hearts and " + str(player.current_space.gems_upgrade_cost) + " gems")
			8: # Wilderness
				match ui.current_menu:
					0:
						ui.update_info("There's a branching path...", "A monster lurks in these parts")
					1:
						ui.update_info("Upgrade to boost rewards gained", "Costs " + str(player.current_space.hearts_upgrade_cost) + " hearts and " + str(player.current_space.gems_upgrade_cost) + " gems")
			9: # Tutorial
				match ui.current_menu:
					0:
						ui.update_info("...")
