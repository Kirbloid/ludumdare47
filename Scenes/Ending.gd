extends Control

onready var main = get_node("/root/Main")
var can_click = false


func _ready():
	match main.ending:
		1:
			$Ending1.visible = true
		2:
			$Ending2.visible = true
		3:
			$Ending3.visible = true


func _process(delta):
	if Input.is_action_just_pressed("click_left") and can_click:
		main.ChangeScene(main._title_screen)
	elif Input.is_action_just_pressed("click_right") and can_click:
		main.ChangeScene(main._game)


func _on_Click_timeout():
	can_click = true
